Note: If running on Windows the server can take about 30 seconds to start up, "Server running on port 8081" will be shown in the terminal when everything is running.

# Requirements
In order to run the node application the following is recommended:
- node (tested on ver 16.4.1)
- npm (tested on ver 7.13.0)
- mysql (tested on ver 8.0.25 for linux)
- Linux OS, or something like mingw for running shell scripts on windows

# Instructions
- Modify `configure-db.sh` with your SQL username and password (or none if auth_socket used), also db.js should be modified with your SQL credentials (lines 6 and 7)
- Run `sudo configure-db.sh` from the root project directory, or manually run the SQL commands inside the script to setup the database **It is important the channel names are added to the database**
- Run `launch.sh` from the projects root directory, or manually run the commands inside of the shell file from the root directory..
- To test the REST api, it is recommended to use postman

# Postman calls
- For task 1a POST `localhost:8081/add`
- For task 1b GET `localhost:8081/get/all`
- For task 2 GET `localhost:8081/get/id`, where id is a number
- For task 3 DELETE `localhost:8081/delete/id`, where id is a name
- For task 4 GET `localhost:8081/search?q=term`, where term is a search term (e.g. dubai)
