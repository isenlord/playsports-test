#!/bin/bash
echo "Setting up database";
mysql -u {username} --password {password} -v -e "CREATE DATABASE mydb;";
mysql -u {username} --password {password} -v -e "source Data/youtube.sql;";
mysql -u {username} --password {password} mydb -v -e "INSERT INTO channels (id, channel_name) VALUES (0, 'GlobalCyclingNetwork'), (1, 'globalmtb');";
# If not using root user, the below line should be run as root to give your user privileges to access mydb
# mysql -u {username} --password {password} -v -e "grant all privileges on mydb.* to 'username';" # Change username
echo "Database setup complete";
