const mysql = require("mysql");

const pool = mysql.createPool({
    host: "localhost",
    database: "mydb",
    user: "test",
    password: "password"
});

let HighestId = 0;

function addVideosToDb(data) {
    
    pool.getConnection((err, connection) => {
        if (err) throw console.error(err);
        
        console.log("Connection established");

        // No point processing the values until we have established a connection
        let values = "";

        for (let i = 0; i < data.length; i++){
            let title = data[i].snippet.title;
            let date = new Date(data[i].snippet.publishedAt).toISOString().slice(0, 19).replace('T', ' ');
            if (i === data.length -1) {
                values += "(" + HighestId + ",'" + title + "', '" + date + "');";    
            } else {
                values += "(" + HighestId + ",'" + title + "', '" + date + "'),";
            }
            HighestId++;
        }

        var query = "INSERT INTO videos(id, title, date) VALUES " + values;
        connection.query(query, (err, result) => {
            if (err) throw console.error(err);

            connection.release();
            console.log(result.affectedRows + " row(s) inserted");
        });
    });
}

module.exports.addVideosToDb = addVideosToDb;
module.exports.pool = pool;