const db = require("./db");
const youtubeapi = require("./youtubeapi");
const express = require("express");
const app = express();


app.listen(8081, () => {
    console.log("Server running on port 8081");
});


app.post("/add", async (req, res) => {
    console.log("Filling database");
    try {
        await youtubeapi.populateDatabase();
    } catch (err) {
        res.send(err);
        console.log(err);
        return;
    }
    res.send("Populating DB");
});

app.get("/get/all", (req, res) => {
    let pool = db.pool;

    console.log("Getting all videos");
    let query = "SELECT * FROM videos";
    pool.query(query, (err, results) => {
        if (err) {
            res.send(err);
            console.log(err);
            return;
        }

        res.send(results);
    });
});

app.get("/get/:id", (req,res) => {
    let pool = db.pool;
    let id = req.params.id;

    if (isNaN(id)) throw console.error("ID is not a number " + id);
    console.log("Getting video with ID " + id);
    let query = "SELECT * FROM videos WHERE id LIKE " + id;
    pool.query(query, (err, result) => {
        if (err) {
            res.send(err);
            console.log(err);
            return;
        }

        res.send(result);
    });
});

app.delete("/delete/:id", (req,res) => {
    let pool = db.pool;
    let id = req.params.id;

    if (isNaN(id)) throw console.error("ID is not a number " + id);
    console.log("Deleting video with ID " + id);
    let query = "DELETE FROM videos WHERE id LIKE " + id;
    pool.query(query, (err, result) => {
        if (err) {
            res.send(err);
            console.log(err);
            return;
        }

        console.log(result.affectedRows + " rows deleted");
        res.send(result);
    });
});

app.get("/search/", (req,res) =>{
    let pool = db.pool;
    let searchTerm = req.query.q;

    console.log("Finding videos with title's matching term: " + searchTerm);

    let query = "SELECT * FROM videos WHERE title LIKE '%" + searchTerm + "%'";
    pool.query(query, (err, results) => {
        if (err) {
            res.send(err);
            console.log(err);
            return;
        }

        let data = '{"items":[';
        for (let i = 0; i < results.length; i++){
            data += '{"id": ' + results[i].id + ', "title":"' + results[i].title + '"}';
            if (i != results.length -1) {
                data += ',';
            }
        }
        data += ']}';
        res.send(JSON.parse(data));
    });
});