const {google} = require("googleapis");
const db = require("./db");
const fs = require("fs");

const youtube = google.youtube({
    version: 'v3',
    auth: 'AIzaSyA9d82tp4S8_vTm7HtJmHSt_3MmHcp1W1Y'
});

async function getChannelId(channelName) {
    let result = await youtube.channels.list({
        "part":[
            "id"
        ],
        "forUsername": channelName
    });
    return result.data.items[0].id;
}

async function retrieveVideos(channelId,filters) {
    console.log("Getting videos for channelId: " + channelId);
    let result = await youtube.search.list({
        "part": [
          "snippet"
        ],
        "channelId": channelId,
        "q": filters
    });

    return result.data.items;
}

async function populateDatabase() { 

    let pool = db.pool;
    let filters = fs.readFileSync("Data/search_filter").toString().replace(/\n/g, "|");
    console.log("Filters loaded: " + filters);

    pool.getConnection((err, connection) => {
        if (err) throw console.error(err);

        console.log("Connection established");

        let query = "SELECT * FROM channels";
        connection.query(query, async (err, results) => {
            if (err) throw console.error(err);
            
            connection.release();
            
            for(let i = 0; i < results.length; i++){
                try {
                let channelId = await getChannelId(results[i].channel_name).catch(console.error);
                let videos = await retrieveVideos(channelId, filters);
                
                console.log("Storing videos for channel: " + channelId);
                db.addVideosToDb(videos);
                } catch (error) { console.log(error); }
            }
        });
    });
    
}

module.exports.populateDatabase = populateDatabase;